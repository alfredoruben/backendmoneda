package com.bcp.reto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bcp.reto.controller.TipoCambioController;

@RunWith(SpringRunner.class)
@SpringBootTest
class RetoBcpTipoCamboApplicationTests {
	@Autowired
	private TipoCambioController controller;
	
	@Test
	void contextLoads() {
		assertThat(controller).isNotNull();
	}

}
