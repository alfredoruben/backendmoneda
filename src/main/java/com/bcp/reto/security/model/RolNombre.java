package com.bcp.reto.security.model;

public enum RolNombre {
    ROLE_ADMIN, ROLE_USER
}
